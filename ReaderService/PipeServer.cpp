#include <windows.h>
#include <vector>
#include "Types.h"
#include "Consts.h"
#include "Utils.h"
#include "PipeHelper.h"
#include "LoaderClient.h"
#include "PipeServer.h"

#define _1K 1024

namespace PipeServer
{
    CPipeServer::CPipeServer()
        : m_ListenerStopEvent(NULL)
        , m_ReaderDLL32(NULL)
        , m_ReadFileByName32 (NULL)
        , m_Loader64Handle (NULL)
        , m_LoaderClientPtr (NULL)
    {
        AddLogEntryA(__FUNCTION__);

        AddLogEntry(L"Load ReaderDLL32");
        m_ReaderDLL32 = LoadLibrary(Consts::ReaderDllFilename32);
        if (NULL == m_ReaderDLL32)
        {
            DisplayErrorSrv(L"Cannot load library!");
        }
        else
        {
            m_ReadFileByName32 = reinterpret_cast<TYPEDEF_ReadFileByName>(GetProcAddress(m_ReaderDLL32, Consts::ReaderDllFunctionName));
            if (NULL == m_ReadFileByName32)
            {
                DisplayErrorSrv(L"Cannot get procedure address (ReadFileByName)!");
                return;
            }
        }

        // start auxiliary loader64 process for call 64bit DLL
        StartLoader64();

        AddLogEntry(L"Start listener thread.");
        m_ListenerStopEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
        CloseHandle(CreateThread(NULL, 0, ListenerThread, static_cast<LPVOID>(this), 0, NULL));
    }

    CPipeServer::~CPipeServer()
    {
        AddLogEntryA(__FUNCTION__);

        StopLoader64() ;
        StopServer();

        if (m_ReaderDLL32)
        {
            FreeLibrary(m_ReaderDLL32);
            m_ReaderDLL32 = NULL;
        }

        if(m_ListenerStopEvent)
        {
            CloseHandle(m_ListenerStopEvent);
            m_ListenerStopEvent = NULL;
        }
    }

    DWORD WINAPI CPipeServer::ListenerThread(LPVOID lParam)
    {
        AddLogEntryA(__FUNCTION__);
        CPipeServer *thisServer = static_cast<CPipeServer*>(lParam);

        HANDLE eventStop = NULL;
        DuplicateHandle(GetCurrentProcess(), thisServer->m_ListenerStopEvent, GetCurrentProcess(), &eventStop,  DUPLICATE_SAME_ACCESS, FALSE, 0);

        while (WaitForSingleObject (eventStop, 0) != WAIT_OBJECT_0)
        {
            thisServer->m_PipeHandle = CreateNamedPipe (Consts::ReaderServiceNamedPipe, 
                PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, 
                PIPE_UNLIMITED_INSTANCES, 512*_1K, 512*_1K, 10000 , NULL);

            if (INVALID_HANDLE_VALUE == thisServer->m_PipeHandle )
            {
                DisplayErrorSrv(L"CreateNamedPipe()");
                return -1;
            }

            while (true)
            {
                AddLogEntry(L"Waiting for client connection...");
                BOOL connectResult = ConnectNamedPipe (thisServer->m_PipeHandle, NULL);
                if(connectResult ? TRUE : GetLastError() == ERROR_PIPE_CONNECTED)
                {
                    AddLogEntry(L"Creating client connection thread");
                    HANDLE connectionThread = CreateThread(NULL, 0, ClientConnectionThread, lParam, 0, NULL);
                    WaitForSingleObject(connectionThread, INFINITE);
                    CloseHandle(connectionThread);
                    continue;
                }
                else
                {
                    DisplayErrorSrv(L"ConnectNamedPipe()");
                    break;
                }
            }

            if ((NULL != thisServer->m_PipeHandle) && (INVALID_HANDLE_VALUE != thisServer->m_PipeHandle))
            {
                AddLogEntry(L"Disconnecting and closing pipe.");
                DisconnectNamedPipe(thisServer->m_PipeHandle);
                CloseHandle(thisServer->m_PipeHandle);
                thisServer->m_PipeHandle = NULL;
            }
        }
        return 0;
    }

    DWORD WINAPI CPipeServer::ClientConnectionThread(LPVOID lParam)
    {
        AddLogEntryA(__FUNCTION__);
        CPipeServer *thisServer = static_cast<CPipeServer*>(lParam);

        // read file name from pipe
        AddLogEntry(L"> Read filename from pipe");
        std::wstring fileName;
        fileName.resize(MAX_PATH);
        if(!thisServer->PopWStr(&fileName.at(0)))
        {
            return -1;
        }

        // read architecture bits parameter
        char archBits = 0;
        thisServer->PopByte(archBits); 

        std::vector<char> fileBuffer(Consts::FileBufferSize);
        char* bufferPtr = fileBuffer.data();
        long readResult = -1;

        // call ReadFile (32 or 64?)
        if ( _32bit == archBits)
        {
            // call ReaderDLL32 directly
            readResult = thisServer->m_ReadFileByName32(fileName.c_str(), bufferPtr);
        }
        else
        {
            // call ReaderDLL64 through Loader64
            readResult = thisServer->ReadFile64(fileName, fileBuffer);
        }

        if(readResult > 0)
        {
            fileBuffer.resize(readResult + 1);
            AddLogEntry(L"> Write file content to the client pipe");
            thisServer->PushData(fileBuffer);
        }
        else
        {
            DisplayErrorSrv(L"ReadFile call");
        }
        return 0;
    }

    void CPipeServer::StopServer()
    {
        AddLogEntryA(__FUNCTION__);
        if(m_ListenerStopEvent)
        {
            SetEvent(m_ListenerStopEvent);
        }
    }

    void CPipeServer::StartLoader64()
    {
        AddLogEntryA(__FUNCTION__);
        STARTUPINFO startupInfo;
        PROCESS_INFORMATION processInfo;
        ZeroMemory( &startupInfo, sizeof(startupInfo));
        startupInfo.cb = sizeof(startupInfo);
        ZeroMemory( &processInfo, sizeof(processInfo) );

        std::wstring filename = (LPWSTR)Consts::Loader64Filename;
        if(CreateProcess (NULL, &filename.at(0),
            NULL, NULL, FALSE, 0, NULL, NULL, &startupInfo, &processInfo))
        {
            AddLogEntryA("Loader64 process created");
            m_Loader64Handle = processInfo.hProcess;
        }
        else
        {
            DisplayErrorSrv(L"Cannot create Loader64 process");
        }

         m_LoaderClientPtr = new CLoaderClient();
    }
    
    void CPipeServer::StopLoader64() 
    {
        AddLogEntryA(__FUNCTION__);
        if(m_LoaderClientPtr)
        {
            delete m_LoaderClientPtr;
            m_LoaderClientPtr = NULL;
        }

        if(WaitForSingleObject(m_Loader64Handle, 0) != WAIT_OBJECT_0)
        {
            TerminateProcess(m_Loader64Handle, 0);
        }
    }

    long CPipeServer::ReadFile64(std::wstring &filename, std::vector<char> &dataBuffer)
    {
        AddLogEntryA(__FUNCTION__);

        if(!m_LoaderClientPtr)
        {
            DisplayErrorSrv(L"Loader error pointer is NULL");
        }
        else
        {
            m_LoaderClientPtr->PushWStr(&filename.at(0));
            m_LoaderClientPtr->PopData(dataBuffer);
        }

        return static_cast<long>(dataBuffer.size());
    }



}