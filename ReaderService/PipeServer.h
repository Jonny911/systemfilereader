#pragma once

namespace PipeServer
{
    class CPipeServer: public PipeHelper::CPipeHelper
    {
    public:
        CPipeServer();
        ~CPipeServer();
        void StopServer();

    private:
        static DWORD WINAPI ListenerThread(LPVOID lParam);
        static DWORD WINAPI ClientConnectionThread(LPVOID lParam);
        void StartLoader64();
        void StopLoader64();
        long ReadFile64(std::wstring &filename, std::vector<char> &dataBuffer);

    private:
        HANDLE m_ListenerStopEvent;
        TYPEDEF_ReadFileByName m_ReadFileByName32;
        HMODULE m_ReaderDLL32;
        HANDLE m_Loader64Handle;
        CLoaderClient *m_LoaderClientPtr;
    };

}