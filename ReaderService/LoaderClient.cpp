#include <windows.h>
#include <vector>
#include "Utils.h"
#include "Consts.h"
#include "PipeHelper.h"
#include "LoaderClient.h"

CLoaderClient::CLoaderClient()
{
    AddLogEntryA(__FUNCTION__);

    m_PipeHandle = NULL;
    for(int i = 1; i < 10; i++)
    {
        if(!WaitNamedPipe(Consts::Loader64NamedPipe, NMPWAIT_USE_DEFAULT_WAIT))
            Sleep(2000);
        else
            break;
    }
    m_PipeHandle = CreateFile(Consts::Loader64NamedPipe, FILE_GENERIC_READ | FILE_GENERIC_WRITE, NULL, NULL, OPEN_EXISTING, 0, NULL);
    if (m_PipeHandle)
    {
        AddLogEntryA("Loader64 pipe opened successful");
    } 
    else
    {
        DisplayErrorSrv(L"Cannot connect to Loader64 pipe");
    }
}

CLoaderClient::~CLoaderClient()
{
    if(m_PipeHandle)
    {
        CloseHandle(m_PipeHandle);
        m_PipeHandle = NULL;
    }
}
