#include <windows.h>
#include <vector>
#include "Types.h"
#include "Utils.h"
#include "PipeHelper.h"
#include "LoaderClient.h"
#include "PipeServer.h"
#include "ReaderService.h"

SERVICE_STATUS g_ServiceStatus = {0};
SERVICE_STATUS_HANDLE g_ServiceStatusHandle = {0};
WCHAR* g_ServiceName = L"SILServiceTest";

HANDLE g_StopEvent = NULL;

int main (int argc, TCHAR *argv[])
{
    AddLogEntry(L"  ======== ENTRYPOINT: main() ENTER");
    SERVICE_TABLE_ENTRY ServiceTable[] = 
    {
        {g_ServiceName, (LPSERVICE_MAIN_FUNCTION) ServiceMain},
        {NULL, NULL}
    };
    if (StartServiceCtrlDispatcher (ServiceTable) == FALSE)
    {
        int errorCode = GetLastError ();
        DisplayErrorSrv(L"StartServiceCtrlDispatcher () ");
        return errorCode;
    }
    return 0;
}

void WINAPI ServiceMain(int argc, char** argv) 
{
    AddLogEntry(L"ServiceMain() ENTER"); 

    g_ServiceStatus.dwServiceType    = SERVICE_WIN32_OWN_PROCESS; 
    g_ServiceStatus.dwCurrentState    = SERVICE_START_PENDING; 

    g_ServiceStatusHandle = RegisterServiceCtrlHandler(g_ServiceName, (LPHANDLER_FUNCTION) ControlHandler); 
    if (NULL == g_ServiceStatusHandle)
    {
        DisplayErrorSrv(L"RegisterServiceCtrlHandler()");
        return;
    }

    g_ServiceStatus.dwControlsAccepted  = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
    g_ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
    SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus);

    HANDLE g_StopEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    HANDLE connectThread = CreateThread(NULL, 0, ConnectThread, static_cast<LPVOID>(g_StopEvent), 0, NULL);

    if(!connectThread)
    {
        DisplayErrorSrv(L"CreateThread()");
        return;
    }

    WaitForSingleObject(connectThread, INFINITE);
    CloseHandle(connectThread);

    g_ServiceStatus.dwControlsAccepted = 0;
    g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
    g_ServiceStatus.dwWin32ExitCode = 0;
    SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus);

    AddLogEntry(L"ServiceMain() EXIT"); 
    return; 
}

DWORD WINAPI ConnectThread(LPVOID lParam)
{
    AddLogEntry(L"Creating PipeServer...");

    PipeServer::CPipeServer pipeServer;

    AddLogEntry(L"Waiting for stop-event...");
    WaitForSingleObject(static_cast<HANDLE>(lParam), INFINITE);
    AddLogEntry(L"Stop-event signal!");

    AddLogEntry(L"StopServer() call.");
    pipeServer.StopServer();

    return 0;
}

void WINAPI ControlHandler(DWORD request) 
{ 
    switch(request) 
    { 
    case SERVICE_CONTROL_STOP: 
    case SERVICE_CONTROL_SHUTDOWN: 
        
        AddLogEntry(L"Stopping SILService."); 

        g_ServiceStatus.dwWin32ExitCode = 0; 
        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED; 
        SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus);

        if(g_StopEvent)
        {
            AddLogEntry(L"Setting stop event"); 
            SetEvent(g_StopEvent);
            CloseHandle(g_StopEvent);
            g_StopEvent = NULL;
        }
        return; 

    default:
        break;
    } 
    SetServiceStatus (g_ServiceStatusHandle, &g_ServiceStatus);
    return; 
} 