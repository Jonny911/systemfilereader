#include <windows.h>
#include <vector>
#include "PipeHelper.h"

namespace PipeHelper
{
    bool CPipeHelper::PushBytes(const char* bytes, ULONG count)
    {
        DWORD written = 0;
        return (WriteFile(m_PipeHandle, bytes, count, &written, NULL) ? true : false);
    }

    bool CPipeHelper::PopBytes (char* bytes, PULONG countBytesPtr)
    {
        return (ReadFile(m_PipeHandle, bytes, *countBytesPtr, countBytesPtr, NULL) ? true : false);
    }

    bool CPipeHelper::PushByte(const char byte)
    {
        return (PushBytes(&byte, sizeof(char)) ? true: false);
    }

    bool CPipeHelper::PopByte(char &byte)
    {
        ULONG count = sizeof(char);
        return (PopBytes(&byte, &count) ? true : false);
    }

    bool CPipeHelper::PushWStr(wchar_t* wstr)
    {
        ULONG sizeOfBuffer = static_cast<ULONG>((wcslen(wstr) + 1 /*(include null terminator)*/) *sizeof(wchar_t));
        // push 4 bytes of string size
        if(PushBytes(reinterpret_cast<char*>(&sizeOfBuffer), sizeof sizeOfBuffer))
        {
            // push bytes of string (include null terminator)
            return (PushBytes(reinterpret_cast<char*>(wstr), sizeOfBuffer) ? true : false);
        }
        return false;
    }

    bool CPipeHelper::PopWStr(wchar_t* wstr)
    {
        std::vector<char> dataBuffer;
        if(PopData(dataBuffer))
        {
            CopyMemory(wstr, dataBuffer.data(), dataBuffer.size());
            return true;
        }
        return false;
    }

    bool CPipeHelper::PushData(const std::vector<char> &dataBuffer)
    {
        ULONG sizeOfData = static_cast<DWORD>(dataBuffer.size());
        if(PushBytes(reinterpret_cast<char*>(&sizeOfData), sizeof sizeOfData))
        {
            return (PushBytes(dataBuffer.data(), sizeOfData) ? true: false);
        }
        return false;
    }

    bool CPipeHelper::PopData(std::vector<char> &dataBuffer)
    {
        ULONG sizeOfData = 0;
        ULONG sizeOfSizeOfData = sizeof sizeOfSizeOfData;
        // get 4 bytes of data size
        if(PopBytes(reinterpret_cast<char*>(&sizeOfData), &sizeOfSizeOfData))
        {
            dataBuffer.resize(sizeOfData);
            ULONG countBytes = sizeOfData;
            // get data bytes
            if(PopBytes(dataBuffer.data(), &countBytes))
            {
                dataBuffer.resize(countBytes);
                return true;
            }
        }
        else
        {
            return false;
        }
        return false;
    }
}
