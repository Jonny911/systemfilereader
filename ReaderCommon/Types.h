#pragma once

typedef long (*TYPEDEF_ReadFileByName) (const wchar_t* name, char* &buffer);

enum ArchValue
{
    _32bit = 1,
    _64bit
};
