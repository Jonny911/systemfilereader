#pragma once

namespace PipeHelper
{
    class CPipeHelper
    {
    public:
        bool PushWStr(wchar_t* wstr);
        bool PopWStr(wchar_t* wstr);

        bool PushData(const std::vector<char> &data);
        bool PopData(std::vector<char> &data);

        bool PushByte(char byte);
        bool PopByte(char &byte);

    protected:
        HANDLE m_PipeHandle;

    private:
        bool PushBytes (const char* bytes, ULONG count);
        bool PopBytes (char* bytes, PULONG countPtr);
    };

}

