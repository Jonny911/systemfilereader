#pragma once

namespace Utils
{
    class CServiceManager
    {
    public:
        CServiceManager();
        ~CServiceManager();
        bool Initialize();
        bool IsServiceInstalled();
        bool IsServiceRinning();
        bool InstallService();
        bool DeleteService();
        bool StartService();
        bool StopService();
        int GetLastError();
    private:
        SC_HANDLE m_scManagerHandle;
        SC_HANDLE m_serviceHandle;
        int m_GastErrorValue;
    };
}

void AddLogEntry(wchar_t* logEntry);
void AddLogEntryA(char* logEntry);

void DisplayErrorSrv(LPTSTR lpszFunction);
DWORD DisplayError(LPTSTR lpszFunction);

#ifdef UNICODE
#define DisplayMessage DisplayMessageW
#else
#define DisplayMessage DisplayMessageA
#endif

void DisplayMessageA(LPSTR text);
void DisplayMessageW(LPWSTR text);





