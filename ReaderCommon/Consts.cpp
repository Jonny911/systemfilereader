#include <windows.h>
#include "Consts.h"

namespace Consts
{
#ifdef _DEBUG
    const wchar_t* ReaderDllFilename32 = L"ReaderDll32d.dll";
    const wchar_t* ReaderDllFilename64 = L"ReaderDll64d.dll";
    const wchar_t* ServiceFilename = L"ReaderService32d.exe";
    const wchar_t* Loader64Filename = L"Loader64d.exe";
#else
    const wchar_t* ReaderDllFilename32 = L"ReaderDll32.dll";
    const wchar_t* ReaderDllFilename64 = L"ReaderDll64.dll";
    const wchar_t* ServiceFilename = L"ReaderService32.exe";
    const wchar_t* Loader64Filename = L"Loader64.exe";
#endif

    const wchar_t* ServiceName = L"SILService";
    const wchar_t* ServiceDisplayName = L"SIL Service for Test task";
    const wchar_t* ServiceLogFilenameExt = L"log";

    const char* ReaderDllFunctionName = "ReadFileByName";
    const wchar_t* FilenameForOpen = L"c:\\windows\\system32\\drivers\\etc\\protocol";

    const DWORD FileBufferSize = 512 * 1024;

    const wchar_t* ReaderServiceNamedPipe = L"\\\\.\\pipe\\SIL_ReaderServicePipe";
    const wchar_t* Loader64NamedPipe = L"\\\\.\\pipe\\SILTestLoaderPipe";

    const wchar_t* InstallServiceParameter = L"-install_service";
}

