#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <vector>
#include <ctime>
#include "Utils.h"
#include "Consts.h"

#include <atlbase.h>
#include <atlsecurity.h>

namespace Utils
{
    CServiceManager::CServiceManager()
        : m_scManagerHandle (NULL)
        , m_serviceHandle (NULL)
        , m_GastErrorValue (0)
    {

    }

    CServiceManager::~CServiceManager()
    {
        if (m_scManagerHandle)
        {
            ::CloseServiceHandle(m_scManagerHandle);
        }
        if (m_serviceHandle)
        {
            this->StopService();
            this->DeleteService();
            ::CloseServiceHandle(m_serviceHandle);
        }
    }

    bool CServiceManager::Initialize()
    {
        m_scManagerHandle = ::OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
        if(m_scManagerHandle)
        {
            return true;
        }
        else
        {
            m_GastErrorValue = ::GetLastError();
            return false;
        }
    }

    bool CServiceManager::IsServiceInstalled()
    {
        if(!m_serviceHandle)
        {
            if(!m_scManagerHandle)
            {
                if(!this->Initialize())
                {
                    return false;
                }
            }
            m_serviceHandle = ::OpenService(m_scManagerHandle, Consts::ServiceName, SERVICE_ALL_ACCESS);
            if(m_serviceHandle)
            {
                return true;
            }
            else
            {
                m_GastErrorValue = ::GetLastError();
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    bool CServiceManager::IsServiceRinning()
    {
        if(this->IsServiceInstalled())
        {
            SERVICE_STATUS serviceStatus;
            if(::QueryServiceStatus(m_serviceHandle, &serviceStatus) && (serviceStatus.dwCurrentState == SERVICE_RUNNING))
            {
                return true;
            }
        }
        return false;
    }

    bool CServiceManager::InstallService()
    {
        if(!m_scManagerHandle) 
        {
            if(!this->Initialize())
            {
                return false;
            }
        }

        if(this->IsServiceInstalled())
        {
            return true;
        }

        std::vector<wchar_t> strPath(MAX_PATH);

        if ( ::GetModuleFileNameW (NULL, &strPath.at(0), static_cast<DWORD>( strPath.capacity() )) )
        {
            std::wstring strSrvExePath(&strPath.at(0), strPath.size());
            std::string::size_type pos = strSrvExePath.rfind('\\');
            strSrvExePath.replace(pos + 1, strSrvExePath.size(), Consts::ServiceFilename);

            m_serviceHandle = ::CreateService(m_scManagerHandle, Consts::ServiceName, Consts::ServiceDisplayName, 
                SERVICE_ALL_ACCESS,  SERVICE_WIN32_OWN_PROCESS, SERVICE_DEMAND_START, 
                SERVICE_ERROR_NORMAL, strSrvExePath.c_str(), NULL, NULL, NULL, NULL, NULL);

            if (m_serviceHandle)
            {
                return true;
            }
        }
        m_GastErrorValue = ::GetLastError();
        return false;
    }

    bool CServiceManager::DeleteService()
    {
        if(!this->IsServiceInstalled())
        {
            return true;
        }

        if(m_serviceHandle)
        {
            if(this->IsServiceRinning())
            {
                this->StopService();
            }
            if(::DeleteService(m_serviceHandle))
            {
                return true;
            }
        }
        m_GastErrorValue = ::GetLastError();
        return false;
    }

    bool CServiceManager::StartService()
    {
        if (IsServiceRinning())
        {
            return true;
        }

        if(!m_serviceHandle)
        {
            if(!m_scManagerHandle) 
            {
                if(!this->Initialize())
                {
                    return false;
                }
            }
            m_serviceHandle = ::OpenService(m_scManagerHandle, Consts::ServiceName, SERVICE_ALL_ACCESS);                        
        }
        if (::StartService(m_serviceHandle, 0,  NULL))
        {
            return true;
        }
        m_GastErrorValue = ::GetLastError();
        return false;
    }

    bool CServiceManager::StopService()
    {
        if (!IsServiceRinning())
        {
            return true;
        }

        if(!m_serviceHandle)
        {
            if(!m_scManagerHandle) 
            {
                if(this->Initialize())
                {
                    return false;
                }
            }
            m_serviceHandle = ::OpenService(m_scManagerHandle, Consts::ServiceName, SERVICE_ALL_ACCESS);                        
        }
        SERVICE_STATUS serviceStatus;
        if(ControlService(m_serviceHandle, SERVICE_CONTROL_STOP, &serviceStatus))
        {
            return true;
        }
        else
        {
            m_GastErrorValue = ::GetLastError();
            return false;
        }
    }

    int CServiceManager::GetLastError()
    {
        return m_GastErrorValue;
    }
}

DWORD DisplayError(LPTSTR lpszFunction) 
{ 
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError(); 

    FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

    lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
    StringCchPrintf((LPTSTR)lpDisplayBuf, LocalSize(lpDisplayBuf) / sizeof(TCHAR), TEXT("%s failed with error %d: %s"), lpszFunction, dw, lpMsgBuf); 

    std::wstring strMessageCaption(TEXT("Error in module: "));

    std::wstring strExePath;
    strExePath.resize(MAX_PATH);
    DWORD resultLength = GetModuleFileNameW(NULL, &strExePath.at(0), static_cast<DWORD>(strExePath.capacity()));
    
    if(resultLength)
    {
        strExePath.resize(resultLength);
    }
    else
    {
        strExePath.clear();
        strExePath = L"UNKNOWN";
    }
    
    strMessageCaption += strExePath;

    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, strMessageCaption.c_str(), MB_OK); 

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    return dw;
}

void DisplayMessageW(LPWSTR text)
{
    std::vector<wchar_t> strExePath(MAX_PATH);
    GetModuleFileNameW(NULL, &strExePath.at(0), static_cast<DWORD>(strExePath.capacity()));

    MessageBox(NULL, (LPCWSTR)text, &strExePath.at(0), MB_OK); 
}

void DisplayMessageA(LPSTR text)
{
    std::vector<char> strExePath(MAX_PATH);
    GetModuleFileNameA(NULL, &strExePath.at(0), static_cast<DWORD>(strExePath.capacity()));

    MessageBoxA(NULL, (LPCSTR)text, &strExePath.at(0), MB_OK); 
}

#pragma warning (disable: 4996)
void AddLogEntry(wchar_t* logEntry)
{
    std::wstring logFullFileName;
    std::vector<wchar_t> strExePath(MAX_PATH);
    if ( GetModuleFileNameW(NULL, &strExePath.at(0), static_cast<DWORD>(strExePath.capacity())) )
    {
        std::wstring strLogPath(&strExePath.at(0), strExePath.size());
        std::string::size_type pos = strLogPath.rfind('.');
        strLogPath.replace(pos + 1, strLogPath.size(), Consts::ServiceLogFilenameExt);
        logFullFileName = strLogPath;
    }
    else
    {
        logFullFileName = L"ReaderService.log";
    }

    HANDLE logHandle = CreateFile(logFullFileName.c_str(), FILE_WRITE_ACCESS, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, 0, NULL);

    if (INVALID_HANDLE_VALUE != logHandle)
    {
        std::time_t t = std::time(NULL);
        wchar_t timeString[50];
        wcsftime(timeString, sizeof(timeString), L"%H:%M:%S ", std::localtime(&t));
        wchar_t buff[50];
        wsprintf(buff, L"%s [TID=%d] ", timeString, GetCurrentThreadId());
        std::wstring strLog;
        strLog.append(buff);
        strLog.append(logEntry);
        strLog.append(L"\r\n");

        SetFilePointer(logHandle, 0, NULL, FILE_END);
        DWORD written = 0;
        WriteFile(logHandle, strLog.c_str(), static_cast<DWORD>(strLog.size() * sizeof(wchar_t)), &written, NULL);
        CloseHandle(logHandle);
    }
}
#pragma warning (default: 4996)

// only for ASCII text
void AddLogEntryA(char* logEntry)
{
    std::string str(logEntry);
    std::wstring strW(str.begin(), str.end());
    AddLogEntry (&strW.at(0));
}


void DisplayErrorSrv(LPTSTR lpszFunction) 
{ 
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError(); 

    FormatMessage (FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

    lpDisplayBuf = (LPVOID) LocalAlloc(LMEM_ZEROINIT, (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR)); 
    StringCchPrintf((LPTSTR)lpDisplayBuf, LocalSize(lpDisplayBuf) / sizeof(TCHAR), TEXT("%s failed with error %d: %s"), lpszFunction, dw, lpMsgBuf); 
    LPTSTR wstr = static_cast<wchar_t*>(lpDisplayBuf);
    wstr[wcslen(wstr) - 1] = L'\0'; // remove redundant "end of line" witch added by FormatMessage()
    wstr[wcslen(wstr)] = L'\0'; // remove redundant "end of line" witch added by FormatMessage()
    AddLogEntry(wstr); 

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
}

