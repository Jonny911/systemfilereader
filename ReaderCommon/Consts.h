#pragma once

namespace Consts
{
    extern const wchar_t* ReaderDllFilename32;
    extern const wchar_t* ReaderDllFilename64;
    extern const wchar_t* ServiceFilename;
    extern const wchar_t* Loader64Filename;

    extern const wchar_t* ServiceName;
    extern const wchar_t* ServiceDisplayName;
    extern const wchar_t* ServiceLogFilenameExt;
    extern const char* ReaderDllFunctionName;
    extern const wchar_t* FilenameForOpen;
    extern const DWORD FileBufferSize;
    extern const wchar_t* ReaderServiceNamedPipe;
    extern const wchar_t* Loader64NamedPipe;

    extern const wchar_t* InstallServiceParameter;

#define _1K 1024

}

