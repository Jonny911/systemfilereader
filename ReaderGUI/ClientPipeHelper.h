#pragma once

class CClientPipeHelper: public PipeHelper::CPipeHelper
{
public:
    CClientPipeHelper();
    ~CClientPipeHelper();
    void OpenPipe(const wchar_t* pipeName);
    long CallFileReader(std::wstring &fileName, std::vector<char> &dataBuffer,  ArchValue archBits);
};
