#pragma once

int WINAPI WinMain ( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd );

BOOL CALLBACK MainDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam);

long CallFileReader(std::wstring &fileName, std::vector<char> &dataBuffer, ArchValue archBits);
