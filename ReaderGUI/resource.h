//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ReaderGUI.rc
//
#define DLG_MAIN                        101
#define ID_CLOSE                        1001
#define IDC_EDIT_FILENAME               1002
#define IDC_BUTTON_BROWSE               1003
#define IDC_RADIO_32                    1004
#define IDC_RADIO_64                    1005
#define IDC_BUTTON_DISPLAYFILE          1006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1007
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
