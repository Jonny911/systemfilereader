#include <windows.h>
#include <tchar.h> 
#include <strsafe.h>
#include <vector>
#include "Types.h"
#include "Consts.h"
#include "Utils.h"
#include "PipeHelper.h"
#include "WinMain.h"
#include "ClientPipeHelper.h"
#include "resource.h"
#include "Elevation.h"

#pragma comment( lib, "comctl32.lib") 

#include <commctrl.h>

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

Utils::CServiceManager g_ServiceManager;
CClientPipeHelper g_ReaderClient;
std::wstring g_CommandLineFileName;

void InstallerCommandLine()
{
    LPWSTR *argsList = NULL;
    int argsCount = 0;

    argsList = CommandLineToArgvW(GetCommandLineW(), &argsCount);

    if( (NULL != argsList) && (3 == argsCount) )
    {
        std::wstring cmdParam(argsList[1]);
        if(0 == cmdParam.compare(Consts::InstallServiceParameter)) // run with parameter for install service
        {
            g_ServiceManager.InstallService();
            g_ServiceManager.StartService();
        }
        g_CommandLineFileName = std::wstring(argsList[2]);
    }
    LocalFree(argsList);
}

int WINAPI WinMain (_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
    InstallerCommandLine();
    InitCommonControls();
    DialogBox(hInstance, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)MainDlgProc);
    return 0;
}

BOOL CALLBACK MainDlgProc(HWND hwndDlg, UINT message, WPARAM wParam, LPARAM lParam) 
{ 
    static bool needToInstallService = false;

    switch (message) 
    { 
    case WM_INITDIALOG:
        SetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_FILENAME), g_CommandLineFileName.empty() ? Consts::FilenameForOpen : &g_CommandLineFileName.at(0));
        CheckDlgButton(hwndDlg, IDC_RADIO_32, BST_CHECKED);

        if (!g_ServiceManager.IsServiceRinning())
        {
            if(g_ServiceManager.GetLastError() == ERROR_ACCESS_DENIED)
            {
                needToInstallService = true;
                SendMessage(GetDlgItem(hwndDlg, IDC_BUTTON_DISPLAYFILE), BCM_SETSHIELD, 0, TRUE);
            }
            else
            {
                g_ServiceManager.InstallService();
                g_ServiceManager.StartService();
            }
        }
        break;

    case WM_COMMAND: 
        switch (LOWORD(wParam)) 
        { 
        case ID_CLOSE: 
            EndDialog(hwndDlg, NULL);
            break;

        case IDC_BUTTON_DISPLAYFILE:
            {
                // get filename from edit control
                std::wstring fileName;
                fileName.resize(MAX_PATH);
                GetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_FILENAME), &fileName.at(0), MAX_PATH);

                // check presence of the file
                if(INVALID_FILE_ATTRIBUTES == GetFileAttributes(fileName.c_str()))
                {
                    DisplayError(&fileName.at(0));
                    break;
                }

                // call reader class with read file from service by the pipe
                std::vector<char> fileBuffer(Consts::FileBufferSize);
                ArchValue archBits = (BST_CHECKED == IsDlgButtonChecked(hwndDlg, IDC_RADIO_32)) ? _32bit : _64bit;

                // restart self process with file name parameter
                if(needToInstallService)
                {
                    // Run self process to install and run service
                    InstallServiceElevated(fileName);
                    EndDialog(hwndDlg, 0);
                    break;
                }

                // Read file by name
                // 32bit call -> service -> dll32
                // 64bit call -> service -> loader64 -> dll64
                long readResult = g_ReaderClient.CallFileReader (fileName, fileBuffer, archBits);

                // display the file content in the message box
                if (readResult != -1 /*common reader error*/)
                {
                    DisplayMessageA(fileBuffer.data());
                }
                else 
                {
                    DisplayMessageA ("Common file read error. Restart the application and try again.");
                }
                break;
            }

        case IDC_BUTTON_BROWSE:
            // prepare OpenFileDialog settings
            wchar_t fileName[MAX_PATH];
            OPENFILENAME openFileName;
            ZeroMemory(&openFileName, sizeof(openFileName));
            openFileName.lStructSize = sizeof(openFileName);
            openFileName.hwndOwner = hwndDlg;
            ZeroMemory(fileName, sizeof fileName);
            openFileName.lpstrFile = fileName;
            openFileName.nMaxFile = sizeof fileName;
            openFileName.lpstrFilter = L"All\0*.*\0Text\0*.TXT\0";
            openFileName.nFilterIndex = 1;
            openFileName.lpstrFileTitle = NULL;
            openFileName.nMaxFileTitle = 0;
            openFileName.lpstrInitialDir = NULL;
            openFileName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

            // show system OpenFileDialog for browse button
            BOOL getFileNameResult = GetOpenFileName(&openFileName);

            if(getFileNameResult)
            {
                // set new filename string to the edit control 
                SetWindowText(GetDlgItem(hwndDlg, IDC_EDIT_FILENAME), openFileName.lpstrFile);
            }
            else
            {
                if(GetLastError() != 0) // Zero GLE value appears for canceling of OpenFileDialog
                    DisplayError(L"GetOpenFileName()");
            }
            break;
        } 
        break;

    case WM_CLOSE:
        EndDialog(hwndDlg, NULL);
        break;
    } 
    return FALSE; 
} 
