#include <windows.h>
#include <vector>
#include "Elevation.h"
#include "Consts.h"

bool InstallServiceElevated(std::wstring &filename)
{
    std::vector<wchar_t> strExePath(MAX_PATH);
    if ( GetModuleFileNameW(NULL, &strExePath.at(0), static_cast<DWORD>(strExePath.capacity())) )
    {
        SHELLEXECUTEINFO shExInfo = {0};
        shExInfo.cbSize = sizeof(shExInfo);
        shExInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
        shExInfo.hwnd = 0;
        shExInfo.lpVerb = L"runas"; 
        shExInfo.lpFile = strExePath.data();
        std::wstring params = Consts::InstallServiceParameter;
        params += L" \""; params += filename; params += L"\"";
        shExInfo.lpParameters = &params.at(0); 
        shExInfo.lpDirectory = 0;
        shExInfo.nShow = SW_SHOW;
        shExInfo.hInstApp = 0;  
        if (ShellExecuteEx(&shExInfo))
        {
            CloseHandle(shExInfo.hProcess);
            return true;
        }
    }
    return false;
 }