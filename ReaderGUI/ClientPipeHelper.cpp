#include <windows.h>
#include <vector>
#include "Types.h"
#include "Utils.h"
#include "Consts.h"
#include "PipeHelper.h"
#include "ClientPipeHelper.h"

CClientPipeHelper::CClientPipeHelper()
{
    m_PipeHandle = NULL;
}

void CClientPipeHelper::OpenPipe(const wchar_t* pipeName)
{
    if(m_PipeHandle != NULL && m_PipeHandle != INVALID_HANDLE_VALUE) 
    {
        return;
    }

    for(int i = 1; i < 10; i++)
    {
        if(!WaitNamedPipe(pipeName, NMPWAIT_USE_DEFAULT_WAIT))
            Sleep(2000);
        else
            break;
    }

    m_PipeHandle = CreateFile(pipeName, FILE_GENERIC_READ | FILE_GENERIC_WRITE, NULL, NULL, OPEN_EXISTING, 0, NULL);
    if (m_PipeHandle == NULL || m_PipeHandle == INVALID_HANDLE_VALUE)
    {
        DisplayErrorSrv(L"Cannot open pipe");
    } 
    else
    {
        AddLogEntryA("Service pipe opened successful");
    }
}

CClientPipeHelper::~CClientPipeHelper()
{
    if(m_PipeHandle && (INVALID_HANDLE_VALUE != m_PipeHandle))
    {
        CloseHandle(m_PipeHandle);
    }
}

long CClientPipeHelper::CallFileReader(std::wstring &fileName, std::vector<char> &dataBuffer,  ArchValue archBits)
{
    AddLogEntryA(__FUNCTION__);

    OpenPipe(Consts::ReaderServiceNamedPipe);

    if (PushWStr(&fileName.at(0))  // send filename string to the service 
        && PushByte(archBits)      // send architecture bits 
        && PopData(dataBuffer))    // get file data from the service
    {
        return static_cast<long>(dataBuffer.size());
    }
    AddLogEntryA(__FUNCTION__" push/pop pipe error");
    return -1;
}

