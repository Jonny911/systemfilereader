#include "Windows.h"
#include <vector>
#include "FileReader.h"
#include "tchar.h"

namespace FileReader
{
    bool CFileReader::ReadFileByNameInternal(std::wstring &fileName, std::vector<char> &fileBuffer)
    {
        HANDLE fileToOpen = CreateFile(fileName.c_str(), FILE_READ_ACCESS, 0, NULL, OPEN_EXISTING, 0, NULL);

        if((NULL != fileToOpen) && (INVALID_HANDLE_VALUE != fileToOpen))
        {
            DWORD fileSize = GetFileSize(fileToOpen, NULL);
            fileBuffer.resize(fileSize);
            ZeroMemory(fileBuffer.data(), fileBuffer.size());

            if (ReadFile(fileToOpen, fileBuffer.data(), fileSize, &fileSize, NULL))
            {
                CloseHandle(fileToOpen);
                return true;
            }
            else
            {
                MessageBox(NULL, _T("Cannot READ file!"), _T("Error"), MB_ICONERROR | MB_OK);
            }
            CloseHandle(fileToOpen);
        }
        else
        {
            MessageBox(NULL, _T("Cannot OPEN file!"), _T("Error"), MB_ICONERROR | MB_OK);
        }
        return false;
    }

}


