#include <windows.h>
#include <vector>
#include "ReadFileFunc.h"
#include "FileReader.h"

namespace ReaderDll
{
    long ReadFileByName (wchar_t* name, char* &buffer)
    {
        std::vector<char> fileBuffer;
        if(FileReader::CFileReader::ReadFileByNameInternal(std::wstring(name), fileBuffer))
        {
            CopyMemory(buffer, fileBuffer.data(), fileBuffer.size());
            return static_cast<long>(fileBuffer.size());
        }
        else
        {
            return -1;
        }
    }
}
