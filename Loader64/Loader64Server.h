#pragma once

class CLoader64Server: public PipeHelper::CPipeHelper
{
public:
    CLoader64Server(HANDLE stopEvent);
    ~CLoader64Server();
    void StopServer();
private:
    static DWORD WINAPI Listener(LPVOID lParam);
    static DWORD WINAPI Connector(LPVOID lParam);
public:
    HANDLE m_StopEvent;
    TYPEDEF_ReadFileByName m_ReadFile;
private:
    HMODULE m_ReaderDLL;
};
