#include <windows.h>
#include <vector>
#include "Types.h"
#include "Consts.h"
#include "Utils.h"
#include "PipeHelper.h"
#include "Loader64Server.h"

CLoader64Server::CLoader64Server(HANDLE stopEvent)
{
    
    m_ReaderDLL = LoadLibrary(Consts::ReaderDllFilename64);
    if (NULL == m_ReaderDLL)
    {
        DisplayErrorSrv(L"Cannot load library!");
    }
    else
    {
        m_ReadFile = reinterpret_cast<TYPEDEF_ReadFileByName>(GetProcAddress(m_ReaderDLL, Consts::ReaderDllFunctionName));
        if (NULL == m_ReadFile)
        {
            DisplayErrorSrv(L"Cannot get procedure address (ReadFileByName)!");
            return;
        }
    }
    AddLogEntry(L"Start listener thread.");
    m_StopEvent = stopEvent;
    CloseHandle(CreateThread(NULL, 0, Listener, static_cast<LPVOID>(this), 0, NULL));
}

DWORD WINAPI CLoader64Server::Listener(LPVOID lParam)
{
    AddLogEntryA(__FUNCTION__);
    CLoader64Server *thisServer = static_cast<CLoader64Server*>(lParam);

    HANDLE eventStop = NULL;
    DuplicateHandle(GetCurrentProcess(), thisServer->m_StopEvent, GetCurrentProcess(), &eventStop,  DUPLICATE_SAME_ACCESS, FALSE, 0);

    while (WaitForSingleObject (eventStop, 0) != WAIT_OBJECT_0)
    {
        thisServer->m_PipeHandle = CreateNamedPipe (Consts::Loader64NamedPipe, 
            PIPE_ACCESS_DUPLEX, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, 
            PIPE_UNLIMITED_INSTANCES, 512*_1K, 512*_1K, 10000 , NULL);

        if (INVALID_HANDLE_VALUE == thisServer->m_PipeHandle )
        {
            DisplayErrorSrv(L"CreateNamedPipe()");
            return -1;
        }

        while (true)
        {
            AddLogEntry(L"Waiting for client connection...");
            BOOL connectResult = ConnectNamedPipe (thisServer->m_PipeHandle, NULL);
            if(connectResult ? TRUE : GetLastError() == ERROR_PIPE_CONNECTED)
            {
                AddLogEntry(L"Creating client connection thread");
                HANDLE connectionThread = CreateThread(NULL, 0, Connector, lParam, 0, NULL);
                WaitForSingleObject(connectionThread, INFINITE);
                CloseHandle(connectionThread);
                continue;
            }
            else
            {
                DisplayErrorSrv(L"ConnectNamedPipe()");
                break;
            }
        }

        if ((NULL != thisServer->m_PipeHandle) && (INVALID_HANDLE_VALUE != thisServer->m_PipeHandle))
        {
            AddLogEntry(L"Disconnecting and closing pipe.");
            DisconnectNamedPipe(thisServer->m_PipeHandle);
            CloseHandle(thisServer->m_PipeHandle);
            thisServer->m_PipeHandle = NULL;
        }
        break;
    }
    SetEvent(eventStop); 
    return 0;
}

DWORD WINAPI CLoader64Server::Connector(LPVOID lParam)
{
    AddLogEntryA(__FUNCTION__);
    CLoader64Server *thisServer = static_cast<CLoader64Server*>(lParam);

    // read file name from pipe
    AddLogEntry(L"> Read filename from pipe");
    std::wstring fileName;
    fileName.resize(MAX_PATH);
    if(!thisServer->PopWStr(&fileName.at(0)))
    {
        return -1;
    }

    std::vector<char> fileBuffer(Consts::FileBufferSize);
    char* bufferPtr = fileBuffer.data();

    long readResult = thisServer->m_ReadFile(fileName.c_str(), bufferPtr);

    if(readResult > 0)
    {
        fileBuffer.resize(readResult + 1);
        AddLogEntry(L"> Write file content to the client pipe");
        thisServer->PushData(fileBuffer);
    }
    else
    {
        DisplayErrorSrv(L"ReadFile call");
    }

    return 0;
}

CLoader64Server::~CLoader64Server()
{
    AddLogEntryA(__FUNCTION__);

    StopServer();

    if (m_ReaderDLL)
    {
        FreeLibrary(m_ReaderDLL);
        m_ReaderDLL = NULL;
    }

    if(m_StopEvent)
    {
        CloseHandle(m_StopEvent);
        m_StopEvent = NULL;
    }
}

void CLoader64Server::StopServer()
{
    AddLogEntryA(__FUNCTION__);
   if(m_StopEvent)
    {
        SetEvent(m_StopEvent);
    }
}