#include <windows.h>
#include <vector>
#include "Types.h"
#include "Consts.h"
#include "Utils.h"
#include "PipeHelper.h"
#include "Loader64Server.h"
#include "Loader64.h"

int WINAPI WinMain ( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd )
{
    AddLogEntryA(__FUNCTION__);
    HANDLE stopEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    CLoader64Server server(stopEvent);
    WaitForSingleObject(stopEvent, INFINITE);
    AddLogEntryA(__FUNCTION__"(): EXIT");
}

